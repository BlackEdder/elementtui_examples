# ElementtuiExamples

Some examples using the [ElementTui](https://gitlab.com/BlackEdder/elementtui) library.

## Usage

To compile and run:

```elixir
mix escript.build
./et_examples [EXAMPLE]
```

Note that when starting your own project you need to make sure elixir/erlang does not listen for input, by setting [-noinput](https://gitlab.com/BlackEdder/elementtui_examples/-/blame/main/mix.exs?ref_type=heads#L14) in the mix.exs file.
