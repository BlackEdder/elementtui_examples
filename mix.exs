defmodule ElementtuiExamples.MixProject do
  use Mix.Project

  def project do
    [
      app: :elementtui_examples,
      version: "0.1.0",
      elixir: "~> 1.16",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      escript: [
        main_module: ElementTuiExamples,
        name: "et_examples",
        emu_args: "-noinput"
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    []
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:optimus, "~> 0.5"},
      # {:elementtui, path: "../elementtui"}
      {:elementtui, "~> 0.2.1"}
    ]
  end
end
