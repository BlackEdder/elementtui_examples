defmodule ElementTuiExamples do
  # Dummy text used in the examples
  @ipsum "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

  alias ElementTui.Element

  def main(argv) do
    args =
      Optimus.new!(
        name: "et_examples",
        args: [
          example: [
            value_name: "EXAMPLE",
            help: "Name of the example",
            required: false,
            default: "hello",
            parser: :string
          ]
        ]
      )
      |> Optimus.parse!(argv)

    case args do
      %{
        args: %{example: "popup"}
      } ->
        # Function to render the screen, it is called in the run_loop function below
        render = fn state ->
          w = div(ElementTui.width(), 2)
          # Columns
          column =
            [
              # White space
              Element.indent(4),
              # Repeat the dummy text ten times and fill a vertical box (vbox) which takes half the width
              List.duplicate(Element.text(@ipsum) |> Element.wrap(), 10)
              |> Element.vbox()
              |> Element.width(w)
            ]
            |> Element.hbox()

          # Show the two columns of text in a horizontal box (hbox)
          base =
            [column, column]
            |> Element.hbox()

          case state do
            %{popup: true} ->
              ln = @ipsum

              # If the popup is active, show the popup with a border in the center of the screen (render area)
              popup =
                Element.text(ln)
                |> Element.width(w)
                |> Element.wrap()
                |> Element.border()
                |> Element.center()

              # Layerbox ensures the popup is on top of the base (the two columns of text)
              Element.layerbox([base, popup])
              |> ElementTui.render(0, 0, ElementTui.width(), ElementTui.height())

            _ ->
              # If popup is not active, show the base
              ElementTui.render(base, 0, 0, ElementTui.width(), ElementTui.height())
          end

          # Draw to the screen
          ElementTui.present()
          # Return the state
          state
        end

        ElementTui.run_loop(
          fn state, ev ->
            case ev do
              {:event, {1, _, _, ?q, _, _, _, _}} ->
                # If q is pressed, exit the loop
                {:halt, nil}

              {:event, {1, _, _, ??, _, _, _, _}} ->
                # If '?' is pressed, enable the popup
                state = render.(%{state | popup: true})
                {:cont, state}

              _ ->
                # Show the state
                state = render.(state)
                {:cont, state}
            end
          end,
          %{popup: false}
        )

      %{
        args: %{example: "twocolumn"}
      } ->
        ElementTui.run_loop(
          fn _, ev ->
            case ev do
              {:event, {1, _, _, ?q, _, _, _, _}} ->
                {:halt, nil}

              _ ->
                w = div(ElementTui.width(), 2)

                column =
                  [
                    Element.indent(4),
                    List.duplicate(Element.text(@ipsum) |> Element.wrap(), 10)
                    |> Element.vbox()
                    |> Element.width(w)
                  ]
                  |> Element.hbox()

                [column, column]
                |> Element.hbox()
                |> ElementTui.render(0, 0, ElementTui.width(), ElementTui.height())

                ElementTui.present()

                {:cont, nil}
            end
          end,
          nil
        )

      _ ->
        ElementTui.run_loop(
          fn _, ev ->
            case ev do
              {:event, {1, _, _, ?q, _, _, _, _}} ->
                {:halt, nil}

              _ ->
                ElementTui.Element.text("Hello World")
                |> ElementTui.render(5, 5, 100, 1)

                ElementTui.present()

                {:cont, nil}
            end
          end,
          nil,
          timeout: 1000
        )
    end
  end
end
